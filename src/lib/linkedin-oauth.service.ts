import { HttpService } from '@nestjs/axios';
import { Injectable, ForbiddenException } from '@nestjs/common';
import { AxiosError, AxiosResponse } from 'axios';
import { catchError, firstValueFrom } from 'rxjs';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class LinkedinOauthService {
  constructor(
    private httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async getRequestToken(authorization_code: string): Promise<AxiosResponse> {
    const body = {
      grant_type: 'authorization_code',
      code: authorization_code,
      client_id: this.configService.get('LINKEDIN_CLIENT_ID'),
      client_secret: this.configService.get('LINKEDIN_CLIENT_SECRET'),
      redirect_uri: this.configService.get('LINKEDIN_CALLBACK_URL'),
    };

    const { data } = await firstValueFrom(
      this.httpService
        .post('https://www.linkedin.com/oauth/v2/accessToken', body, {
          headers: { 'content-type': 'application/x-www-form-urlencoded' },
        })
        .pipe(
          catchError((error: AxiosError) => {
            console.log(error.response);
            throw new ForbiddenException('Invalid Request');
          }),
        ),
    );

    return data;
  }

  async getUser(accessToken: string): Promise<AxiosResponse> {
    const { data } = await firstValueFrom(
      this.httpService
        .get('https://api.linkedin.com/v2/me', {
          headers: {
            Authorization: accessToken,
            // 'X-RestLi-Protocol-Version': '2.0.0',
          },
        })
        .pipe(
          catchError((error: AxiosError) => {
            console.log(error.response);
            throw new ForbiddenException('Invalid Request');
          }),
        ),
    );
    return data;
  }
}
