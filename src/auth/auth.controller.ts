import {
  Controller,
  Post,
  Get,
  Body,
  HttpException,
  HttpStatus,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LinkedinLoginDto } from './dto/linkedin-login.dto';
import { AuthGuard } from './auth.guard';
import { Request } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('linkedin/request_token')
  async handleLinkedinAuthToken(@Body() linkedinLoginDto: LinkedinLoginDto) {
    try {
      const result =
        await this.authService.linkedinRequestToken(linkedinLoginDto);
      return result;
    } catch (error) {
      console.log(error);
      throw new HttpException(
        error.message,
        error.statusCode || error.code || HttpStatus.PRECONDITION_FAILED,
      );
    }
  }

  @UseGuards(AuthGuard)
  @Get('/me/profile')
  async getUserProfile(@Req() req: Request) {
    const token = req.headers.authorization;
    try {
      const result = await this.authService.getUserProfile(token);
      return result;
    } catch (error) {
      console.log(error);
      throw new HttpException(
        error.message,
        error.statusCode || error.code || HttpStatus.PRECONDITION_FAILED,
      );
    }
  }
}
