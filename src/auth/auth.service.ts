import { Injectable } from '@nestjs/common';
import { LinkedinLoginDto } from './dto/linkedin-login.dto';
import LinkedinOauthService from 'src/lib/linkedin-oauth.service';

@Injectable()
export class AuthService {
  constructor(private readonly linkedinOauthService: LinkedinOauthService) {}

  async linkedinRequestToken(linkedinLoginDto: LinkedinLoginDto) {
    const data = await this.linkedinOauthService.getRequestToken(
      linkedinLoginDto.authorization_code,
    );

    // Perform business logic after getting access token

    return data;
  }

  async getUserProfile(accessToken: string) {
    const data = await this.linkedinOauthService.getUser(accessToken);
    return data;
  }
}
