import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import LinkedinOauthService from '../lib/linkedin-oauth.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [AuthService, LinkedinOauthService],
  controllers: [AuthController],
})
export class AuthModule {}
