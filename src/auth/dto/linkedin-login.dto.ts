import { IsString, IsNotEmpty } from 'class-validator';

export class LinkedinLoginDto {
  @IsNotEmpty()
  @IsString()
  authorization_code: string;
}
